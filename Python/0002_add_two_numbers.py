# Time:  O(n)
# Space: O(1)
from unittest import TestCase


class ListNode(object):
    def __init__(self, x, next=None):
        self.val = x
        self.next = next


class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        dummy = ListNode(0)
        current, carry = dummy, 0

        while l1 or l2:
            val = carry
            if l1:
                val += l1.val
                l1 = l1.next
            if l2:
                val += l2.val
                l2 = l2.next
            carry, val = divmod(val, 10)
            current.next = ListNode(val)  # Avoid deciding whether to create new node
            current = current.next

        if carry == 1:
            current.next = ListNode(1)

        return dummy.next  # Skip the dummy root


class Test(TestCase):
    def test_simple(self):
        list1 = ListNode(2, ListNode(4, ListNode(3)))

        list2 = ListNode(5, ListNode(6, ListNode(4)))

        true_list = ListNode(7, ListNode(0, ListNode(8)))

        test_list = Solution().addTwoNumbers(list1, list2)

        while test_list:
            self.assertEqual(true_list.val, test_list.val)
            true_list = true_list.next
            test_list = test_list.next
