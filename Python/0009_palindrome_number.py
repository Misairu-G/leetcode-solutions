# Time:  O(1)
# Space: O(1)
from unittest import TestCase


class Solution(object):
    def isPalindrome(self, x: int) -> bool:
        # Runtime, 36%
        if x < 0:
            return False

        copy, reverse = x, 0

        while copy:
            reverse *= 10
            reverse += copy % 10
            copy //= 10

        return x == reverse

    def strPalindrome(self, x: int) -> bool:
        # Runtime, 97%
        s = str(x)
        return s == s[::-1]

    def myPalindrome(self, x: int) -> bool:
        # Runtime, 27%
        if x < 0:
            return False

        remain, reverse = x, 0
        while remain:
            reverse *= 10
            remain, mod = divmod(remain, 10)  # divmod is slower, why?
            reverse += mod

        return x == reverse


class Test(TestCase):
    def setUp(self):
        self.func = Solution().myPalindrome

    def test_simple(self):
        self.assertEqual(self.func(121), True)
