# Time:  O(n)
# Space: O(1)
from unittest import TestCase
from collections import OrderedDict
from typing import Dict


class Solution(object):
    def lengthOfLongestSubstring(self, s: str) -> int:
        longest, start, visited = 0, 0, [False for _ in range(256)]
        for i, char in enumerate(s):
            if visited[ord(char)]:
                while char != s[start]:
                    visited[ord(s[start])] = False
                    start += 1
                start += 1
            else:
                visited[ord(char)] = True
            longest = max(longest, i - start + 1)
        return longest

    def lengthOfLongestSubstringFast(self, s):
        dct: Dict[str: int] = {}  # {char: index}
        max_so_far = curr_max = start = 0
        for index, char in enumerate(s):
            if char in dct and dct[char] >= start:
                max_so_far = max(max_so_far, curr_max)
                curr_max = index - dct[char]
                start = dct[char] + 1
            else:
                curr_max += 1
            dct[char] = index
        return max(max_so_far, curr_max)

    def myLengthOfLongestSubstring(self, s: str) -> int:
        """Use hash table to quickly check index, and maintain order with ordered dict"""
        longest = 0
        lookup = OrderedDict()
        for c in s:
            if c not in lookup:
                lookup[c] = True
            else:
                longest = max(longest, len(lookup))
                del_end = 0
                for i, char in enumerate(lookup):
                    if char == c:
                        del_end = i
                        break
                for i in range(del_end + 1):
                    lookup.popitem(last=False)
                lookup[c] = True

        longest = max(longest, len(lookup))
        return longest


class Test(TestCase):
    def setUp(self):
        self.func = Solution().myLengthOfLongestSubstring

    def test_simple(self):
        self.assertEqual(self.func("abcabcbb"), 3)
        self.assertEqual(self.func(" "), 1)
        self.assertEqual(self.func("advdaf"), 4)
        self.assertEqual(self.func("pwwkew"), 3)
        self.assertEqual(self.func("aabaab!bb"), 3)
