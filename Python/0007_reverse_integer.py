# Time:  O(logn) = O(1)
# Space: O(1)
from filecmp import cmp
from unittest import TestCase


class Solution(object):
    def reverse(self, x: int) -> int:
        """
        This solution is not very good since
        """
        if x < 0:
            return -self.reverse(-x)

        result = 0
        while x:
            result = result * 10 + x % 10
            x //= 10
        return result if result <= 0x7fffffff else 0  # Handle overflow.

    def reverse2(self, x: int) -> int:
        """
        """
        if x < 0:
            x = int(str(x)[::-1][-1] + str(x)[::-1][:-1])
        else:
            x = int(str(x)[::-1])
        x = 0 if abs(x) > 0x7FFFFFFF else x
        return x

    def reverse3(self, x: int) -> int:
        """
        """
        s = cmp(x, 0)
        r = int(repr(s * x)[::-1])
        return s * r * (r < 2 ** 31)

    def myReverse(self, x: int) -> int:
        return -x


class Test(TestCase):
    def setUp(self):
        self.func = Solution().reverse

    def test_simple(self):
        self.assertEqual(self.func(123), 321)
